<?php get_header();?>

<?php
$qPortfolio = new WP_Query([
    'post_type' => 'portfolio',
    'order' => 'ASC'
])
?>

<div class="container">
    <div class="wrap">
    <?php if ( $qPortfolio->have_posts() ) : while ( $qPortfolio->have_posts() ) :    $qPortfolio->the_post(); ?>
        <div class="wrap-item ">
            <a href="<?php the_permalink();?>" class="portfolio-link">
                <!-- portfolio -->    
                <h3 class="portfolio-head"> <?php the_title();?></h3>
                <img class="portfolio-mini" src="<?php the_post_thumbnail_url( 'thumbnail' );  ?>" alt="mini">
                <?php the_excerpt(); ?>
            </a>
        </div>
        <!--end-wrap-item -->
                <?php endwhile; ?>
                    <!-- post navigation -->
                <?php else: ?>
                    <!-- no posts found -->
                <?php endif; ?>
            
    </div>
</div>
</div>
</div><!--end-container -->
<?php get_footer(); ?>
