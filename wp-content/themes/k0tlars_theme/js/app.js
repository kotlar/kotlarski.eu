jQuery(function($){
    console.log('dziala');

    var js = $('.javascript');
    var php = $('.php');
    var html = $('.htmlcss');

    var cjs = $('.content-js');
    var cphp = $('.content-php');
    var chtml = $('.content-htmlcss');


    //navigation 
    var openMenu = $('.nav-ham');
    var logo = $('.logo');
    var closeMenu = $('.close-menu');
    var overlay = $('.header-nav-overlay');

    openMenu.on('click', function(){
        overlay.css("display", "flex");
        logo.css("display","none");
    });

    closeMenu.on('click', function(){
        overlay.css("display", "none");
    });

    js.on('click', function(){
        cjs.css("display", "flex");
        cphp.css("display", "none");
        chtml.css("display", "none");
    });

    php.on('click', function(){
        cjs.css("display", "none");
        cphp.css("display", "flex");
        chtml.css("display", "none");
    });

    html.on('click', function(){
        cjs.css("display", "none");
        cphp.css("display", "none");
        chtml.css("display", "flex")
    });

   
    const slider =  new Siema({
    duration: 400,
    easing: 'ease-out',
    perPage: 1,
    loop: true,
});
$('.prev-btn').on('click', function(){
    slider.prev();
});
$('.next-btn').on('click', function(){
    slider.next();
})
 

});


