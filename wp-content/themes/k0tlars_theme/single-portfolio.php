<?php get_header(); ?>



<div class="container">
    <?php if ( have_posts() ) : while ( have_posts() ) :    the_post(); ?>

        <!-- post -->
        <h2 class="portfolio-head"><?php the_title();?></h2>
        <div class="wrap">
            <a href="<?php the_field('link')?>">
                <img class="single-portfolio-mini" src="<?php the_post_thumbnail_url();  ?>" alt="mini">
            </a>
        <?php the_content(); ?>

    <?php endwhile; ?>
        <!-- post navigation -->
    <?php else: ?>
        <!-- no posts found -->
    <?php endif; ?>
    </div>
</div>


<?php get_footer(); ?>