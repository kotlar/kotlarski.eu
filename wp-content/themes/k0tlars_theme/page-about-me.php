<?php get_header();?>

<div class="container">
    <?php if ( have_posts() ) : while ( have_posts() ) :    the_post(); ?>
        <!-- post -->
        <div class="about-title">
          <h2><?php the_title();?></h2>
        </div>
        <div class="main-about">
            <div class="main-img-about">

            </div>

            <div class="main-content-about">
            <?php the_content(); ?>
            </div>
        </div>
    <?php endwhile; ?>
        <!-- post navigation -->
    <?php else: ?>
        <!-- no posts found -->
    <?php endif; ?>
</div>
<?php get_footer(); ?>
