<?php

/**
 * Registers the `portfolio` post type.
 */
function portfolio_init() {
	register_post_type( 'portfolio', array(
		'labels'                => array(
			'name'                  => __( 'Portfolios', 'kotlar' ),
			'singular_name'         => __( 'Portfolio', 'kotlar' ),
			'all_items'             => __( 'All Portfolios', 'kotlar' ),
			'archives'              => __( 'Portfolio Archives', 'kotlar' ),
			'attributes'            => __( 'Portfolio Attributes', 'kotlar' ),
			'insert_into_item'      => __( 'Insert into portfolio', 'kotlar' ),
			'uploaded_to_this_item' => __( 'Uploaded to this portfolio', 'kotlar' ),
			'featured_image'        => _x( 'Featured Image', 'portfolio', 'kotlar' ),
			'set_featured_image'    => _x( 'Set featured image', 'portfolio', 'kotlar' ),
			'remove_featured_image' => _x( 'Remove featured image', 'portfolio', 'kotlar' ),
			'use_featured_image'    => _x( 'Use as featured image', 'portfolio', 'kotlar' ),
			'filter_items_list'     => __( 'Filter portfolios list', 'kotlar' ),
			'items_list_navigation' => __( 'Portfolios list navigation', 'kotlar' ),
			'items_list'            => __( 'Portfolios list', 'kotlar' ),
			'new_item'              => __( 'New Portfolio', 'kotlar' ),
			'add_new'               => __( 'Add New', 'kotlar' ),
			'add_new_item'          => __( 'Add New Portfolio', 'kotlar' ),
			'edit_item'             => __( 'Edit Portfolio', 'kotlar' ),
			'view_item'             => __( 'View Portfolio', 'kotlar' ),
			'view_items'            => __( 'View Portfolios', 'kotlar' ),
			'search_items'          => __( 'Search portfolios', 'kotlar' ),
			'not_found'             => __( 'No portfolios found', 'kotlar' ),
			'not_found_in_trash'    => __( 'No portfolios found in trash', 'kotlar' ),
			'parent_item_colon'     => __( 'Parent Portfolio:', 'kotlar' ),
			'menu_name'             => __( 'Portfolios', 'kotlar' ),
		),
		'public'                => true,
		'hierarchical'          => false,
		'show_ui'               => true,
		'show_in_nav_menus'     => true,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'link', 'excerpt' ),
		'has_archive'           => true,
		'rewrite'               => true,
		'query_var'             => true,
		'menu_icon'             => 'dashicons-admin-post',
		'show_in_rest'          => true,
		'rest_base'             => 'portfolio',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'portfolio_init' );

/**
 * Sets the post updated messages for the `portfolio` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `portfolio` post type.
 */
function portfolio_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['portfolio'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Portfolio updated. <a target="_blank" href="%s">View portfolio</a>', 'kotlar' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'kotlar' ),
		3  => __( 'Custom field deleted.', 'kotlar' ),
		4  => __( 'Portfolio updated.', 'kotlar' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Portfolio restored to revision from %s', 'kotlar' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Portfolio published. <a href="%s">View portfolio</a>', 'kotlar' ), esc_url( $permalink ) ),
		7  => __( 'Portfolio saved.', 'kotlar' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Portfolio submitted. <a target="_blank" href="%s">Preview portfolio</a>', 'kotlar' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Portfolio scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview portfolio</a>', 'kotlar' ),
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Portfolio draft updated. <a target="_blank" href="%s">Preview portfolio</a>', 'kotlar' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'portfolio_updated_messages' );
