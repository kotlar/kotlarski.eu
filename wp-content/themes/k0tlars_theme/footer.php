<footer>
    <div class="container">
        <div class=" links">
            <p class="cpyright">&copy; 2019 | <?php echo bloginfo('author'); ?></p>
            <div class="logos">
                <div class="fb sign"></div>
                <div class="in sign">
                    <span></span>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
<script type="text/javascript" src="http://ciasteczka.org.pl/cookies/cookiesstandard.txt"></script>
</body>
</html>