<?php get_header();?>

<div class="container">
    <?php if ( have_posts() ) : while ( have_posts() ) :    the_post(); ?>
    <h2 class="contact-head"><?php the_title();?></h2>
    <div class="formularz">
            <!-- post -->

        <?php the_content(); ?>
        <div class="rightContent">
            <p>
                Napisz do mnie!
            </p>
        </div>
        </div>
    <?php endwhile; ?>
        <!-- post navigation -->
    <?php else: ?>
        <!-- no posts found -->
    <?php endif; ?>
</div>
<?php get_footer(); ?>

