<?php get_header(); ?>


    <div class="container">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <!-- post -->
            <h2><?php the_title(); ?></h2>
            single
            <?php the_content(); ?>
            <?php post_thumbnails();?>
        <?php endwhile; ?>
            <!-- post navigation -->
        <?php else: ?>
            <!-- no posts found -->
        <?php endif; ?>
    </div></div></div>


<?php get_footer(); ?>