<?php get_header(); ?>


 <div class="main">
    <div class="container">
        <div class="main-hero">
        <div class="siema">
            <div class="slide">
                <h2 class="slide-title">Chcesz zaistnieć w sieci?</h2>
                <p class="slide-content">Potrzebujesz do tego strony.</p>
            </div>
            <div class="slide">
                <h2 class="slide-title-2">Kim jestem?</h2>
                <img src="" alt="">
                <div class="slide-content">Nazywam się Michał i jestem Web deweloperem. Swoją przygodę z programowaniem zacząłem jakieś półtora roku temu, każdego dnia rozwijam swoje umiejętności, aby być jeszcze lepszym w tym co robię. Wiecej dowiesz się na stronie <a class="direct-link" href="http://localhost/kotlarskieu/about-me/">o mnie</a> </div>
            </div>
            <div class="slide">
                <h2 class="slide-title">Co mogę dla ciebie zrobić</h2>
                <div class="slide-content">Stworzyć stronę jaka potrzebujesz.</div>
            </div>
            </div>
            <span class="prev-btn">prev</span>
            <span class="next-btn">next</span>
        </div>
        <div class="abilities javascript">Javascript</div>
        <div class="abilities php">PHP</div>
        <div class="abilities htmlcss">HTML\CSS</div>
    </div>

    <div class="content">
        <div class="container">
            <div class="content-js">
                <div class="logo-language">
                    <img class="img-js" src="./wp-content/themes/k0tlars_theme/img/js-logo.png" alt="">
                </div>
                <div class="content-language"></div>
            </div>
            <div class="content-php">
                <div class="logo-language">
                    <img class="img-php" src="./wp-content/themes/k0tlars_theme/img/php.png" alt="">
                </div>
                <div class="content-language"></div>
                </div>
                <div class="content-htmlcss">
                <div class="logo-language">
                    <img class="img-html" src="./wp-content/themes/k0tlars_theme/img/htmlcss.png" alt="">
                </div>
                    <div class="content-language"></div>
                </div>
            </div>
        </div>
    </div>    
</div> 

<script type="text/javascript" src="./wp-content/themes/k0tlars_theme/js/siema.min.js"></script>


<?php get_footer(); ?>
