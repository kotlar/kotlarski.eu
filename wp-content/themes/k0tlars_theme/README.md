<img alt="Logo" src="http://coderslab.pl/svg/logo-coderslab.svg" width="400">

# Wordpress | Routing

### Zadania wykonywane z wykładowcą
1. Zainstaluj i włącz wtyczkę [https://pl.wordpress.org/plugins/what-the-file/](https://pl.wordpress.org/plugins/what-the-file/). Przetestuj jej działanie.
2. W panelu administracyjnym ```Ustawienia > Bezpośrednie odnośniki``` wybierz typ prosty.
3. Zainstaluj wtyczkę [https://pl.wordpress.org/plugins/fakerpress/](https://pl.wordpress.org/plugins/fakerpress/). Dzięki niej łatwo dodasz nowe masowo treści do Twojego WordPress'a. Dodaj kilka postów i stron oraz kilka kategorii i tagów. Automatycznie przyporządkuj kategorie i tagi do postów.
    ### Zadania do samodzielnego wykonania
4. W pliku ```index.php``` znajduje się zapytanie do bazy danych WordPress'a pobierające wszystkie rekordy z tabeli ```prefix_posts```. Do komunikacji z bazą danych WordPress używa spacjalnej klasy ```WPDB```. Przyjrzyj się zaimplementowanemu rozwiązaniu a następnie stwórz zapytanie do bazy danych WordPress'a które wyświeli tytuł – ```post_title``` i główną treść ```post_content``` aktualnego wpisu.
    ##### Nie wiesz jaki jest aktualny wpis? Zajrzyj do adresu url widoku któregoś z wpisów i odnajdź parametr ```p```.
    ##### Aby pobrać parametr użyj zmiennej superglobalnej ```$_GET```.  

    ```php
    $p;
    if($_SERVER['REQUEST_METHOD'] === "GET") {
       if(isset($_GET['p'])) {
           $p = trim($_GET['p']);
       }
    }
    ```
    
    Pamiętaj, że parametr p odpowiada kolumnie ```ID``` w bazie danych. Przetestuj swoje rozwiązanie.
    
5. Spróbuj teraz w podobny sposób pobrać z bazy danych aktualną stronę. W tym celu musisz odczytać parametr ```post_id``` z adresu url widoku strony. 
    
    #### Dla chętnych 

6. Do czwartego zadania dopisz klauzulę ```JOIN```, która obok tytułu i treści wpisu pobierze a następnie wyrenderuje tytuły przyporządkowanych do niego kategorii.

##### Uwaga!!! pisanie własnych zapytań do bazy danych WordPress'a nie jest najlepszym rozwiązaniem. WordPress ma do tego gotowe metody, których nauczysz się w następnym rodziale. Dzięki powyższym ćwiczeniom będziesz jednak rozumiał na czym polega routing.

